(function (window, document) {
  function debug (str) {
    var c = document.querySelector('.sidebar-log');
    if (c) {
      c.innerHTML = str;
    }
  }

  /**
   * Create a sidebar which is always in view no matter the positioning within
   * the viewport nor the height of the content.
   */
  var sidebar = function (containerSelector, contentSelector, options) {
    options = options ? options : {};

    var self = this;
    var last = 0;
    var ticking = false;
    var world = {
      forced: options.forced || true,
      tearing: options.tearing || false,
      tolerance: options.tolerance || 110,
      lastScrollTop: 0,
      direction: 'DOWN',
      boundaries: {}
    };

    var workspace = options.workspace ? document.querySelector(options.workspace) : window;
    var container = document.querySelector(containerSelector);
    var content = document.querySelector(contentSelector);

    workspace.addEventListener('scroll', function (ev) {
      if (canBeSticky(container, content)) {
        last = window.scrollY;
        if (!ticking) {
          ticking = true;

          // Direction
          var sd = scrollDirection(workspace, world.lastScrollTop);
          world.lastScrollTop = sd.scrollTop;
          world.direction = sd.direction;

          // Boundaries
          world.boundaries = boundaries(container, content);

          // Positioning
          applyPositioning(container, content, world);
          ticking = false;

          debug(
            'world lastScrollTop: ' + world.lastScrollTop + '<br/>' +
              'world direction: ' + world.direction + '<br/>' +
              'world boundaries containerTop: ' + world.boundaries.containerTop + '<br/>' +
              'world boundaries containerHeight: ' + world.boundaries.containerHeight + '<br/>' +
              'world boundaries containerBottom: ' + world.boundaries.containerBottom + '<br/>' +
              'world boundaries contentHeight: ' + world.boundaries.contentHeight + '<br/>' +
              'world boundaries windowHeight: ' + world.boundaries.windowHeight + '<br/>' +
              'positioning applied' + '<br/>' +
              'content position: ' + content.style.position + '<br/>' +
              'content top: ' + content.style.top + '<br/>' +
              'content class: ' + content.className);
        }
      }
    });
  }

  /**
   * Basic test to see if the content qualifies for
   * sticky application.
   */
  function canBeSticky (container, content) {
    var outer = height(container);
    var inner = height(content);
    var win = window.innerHeight;
    return inner < outer && inner >= win;
  }

  /**
   * Determine scroll direction and current top.
   */
  function scrollDirection (workspace, previous) {
    var workspaceScrollTop = scrollTop(workspace);
    var direction = workspaceScrollTop > previous ? 'DOWN' : 'UP';
    return {
      scrollTop: workspaceScrollTop,
      direction: direction
    };
  }

  /**
   * Computes world boundaries.
   */
  function boundaries (container, content) {
    var containerTop = top(container);
    var containerHeight = container.offsetHeight;
    var contentHeight = content.offsetHeight;
    var windowHeight = window.innerHeight;
    return {
      containerTop: containerTop,
      containerBottom: containerTop + containerHeight,
      containerHeight: containerHeight,
      contentHeight: contentHeight,
      windowHeight: windowHeight
    };
  }

  /**
   * Apply positioning to the content based on
   * the current world view.
   */
  function applyPositioning (container, content, world) {
    if (world.lastScrollTop > world.boundaries.containerTop
        && world.lastScrollTop < world.boundaries.containerBottom) {
      if (world.direction === 'DOWN') {
        scrollDown(container, content, world);
      } else {
        scrollUp(container, content, world);
      }
    } else if (world.lastScrollTop < world.boundaries.containerTop) {
      content.style.top = '';
      removeClass(content, 'sidebar-top-fixed');
    }
  }

  /**
   * Handles when scrolling down
   */
  function scrollDown(container, content, world) {
    var windowScroll = world.lastScrollTop + world.boundaries.windowHeight;

    if (hasClass(content, 'sidebar-scrolling-up')) {
      removeClass(content, 'sidebar-scrolling-up');
      addClass(content, 'sidebar-scrolling-down');
    } else if (hasClass(content, 'sidebar-top-fixed')) {
      var contentOffsetTop = top(content) + world.boundaries.containerTop;
      removeClass(content, 'sidebar-top-fixed');
      content.style.position = 'absolute';
      content.style.top = '' + contentOffsetTop + 'px';
      addClass(content, 'sidebar-scrolling-down');
    }

    if (hasClass(content, 'sidebar-scrolling-down')) {
      if (windowScroll > top(content) + world.boundaries.contentHeight) {
        content.style.position = '';
        content.style.top = '';
        addClass(content, 'sidebar-bottom-fixed');
        removeClass(content, 'sidebar-scrolling-down');
      }
    } else {
      if (world.tearing && windowScroll > world.boundaries.containerBottom) {
        removeClass(content, 'sidebar-bottom-fixed');
        content.style.position = 'absolute';
        content.style.top = '' + (world.boundaries.containerHeight - world.boundaries.contentHeight) + 'px';
      } else if (windowScroll + world.tolerance > world.boundaries.contentHeight + world.boundaries.containerTop) {
        content.style.position = '';
        content.style.top = '';
        removeClass(content, 'sidebar-top-fixed');
        addClass(content, 'sidebar-bottom-fixed');
      }
    }
  }

  /**
   * Handles when scrolling up
   */
  function scrollUp(container, content, world) {
    if (hasClass(content, 'sidebar-scrolling-down')) {
      removeClass(content, 'sidebar-scrolling-down');
      addClass(content, 'sidebar-scrolling-up');
    } else if (hasClass(content, 'sidebar-bottom-fixed')) {
      var contentOffsetTop = top(content) - world.boundaries.containerTop;
      content.style.position = 'absolute';
      content.style.top = '' + contentOffsetTop + 'px';
      removeClass(content, 'sidebar-bottom-fixed');
      addClass(content, 'sidebar-scrolling-up');
    }

    if (hasClass(content, 'sidebar-scrolling-up')) {
      // if (world.lastScrollTop < top(container)) {
      if (top(content) > 0) {
        content.style.position = '';
        content.style.top = '';
        addClass(content, 'sidebar-top-fixed');
        removeClass(content, 'sidebar-scrolling-up');
      }
    } else {
      if (world.tearing && world.lastScrollTop > world.boundaries.containerTop) {
        content.style.position = '';
        removeClass(content, 'sidebar-top-fixed');
      } else if (world.lastScrollTop - world.tolerance < world.boundaries.containerBottom - world.boundaries.contentHeight) {
        content.style.position = '';
        content.style.top = '';
        addClass(content, 'sidebar-top-fixed');
        removeClass(content, 'sidebar-scrolling-up');
      }
    }
  }

  /**
   * Return element offset.
   */
  function offset (element) {
    if (element) {
      var rect = element.getBoundingClientRect();
      return {
        top: rect.top + document.body.scrollTop,
        left: rect.left + document.body.scrollLeft
      };
    }
    return { top: 0, bottom: 0 };
  }

  /**
   * Return element computed height.
   */
  function height (element) {
    var style = window.getComputedStyle(element, null);
    var h = parseInt(style.height);
    if (isNaN(h)) {
      h = parseInt(style['min-height']);
    }
    return isNaN(h) ? 0 : h;
  }

  /**
   * Return element top position.
   */
  function top (element) {
    return offset(element).top;
  }

  /**
   * Return the scroll top position.
   */
  function scrollTop (element) {
    if (element.scrollTop) {
      return element.scrollTop;
    }

    if (window.pageYOffset !== undefined) {
      return window.pageYOffset;
    }

    return (document.documentElement || document.body.parentNode || document.body).scrollTop;
  }

  /**
   * Add a class to the element.
   */
  function addClass(element, className) {
    if (element.classList) {
      element.classList.add(className);
    } else {
      element.className += ' ' + className;
    }
  }

  /**
   * Remove a class from the element.
   */
  function removeClass(element, className) {
    if (element.classList) {
      element.classList.remove(className);
    } else {
      element.className = element.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
    }
  }

  /**
   * Determine if element has a class name applied.
   */
  function hasClass(element, className) {
    if (element.classList) {
      return element.classList.contains(className);
    }
    return new RegExp('(^| )' + className + '( |$)', 'gi').test(element.className);
  }

  window.sidebar = sidebar;
})(window, document);
